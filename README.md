# Two Bulbs (2013)

Artwork by [Tobias Zehntner](http://www.tobiaszehntner.art)

- Artwork ©2013 Tobias Zehntner
- Code licensed as Open Source: [APGL-3.0](https://www.gnu.org/licenses/agpl.html)
- [Documentation](http://www.tobiaszehntner.art/work/two-bulbs)
