of// This code will blink output 13 every 250 ms
// abd will blink output 9 every 125 ms


#include <Metro.h> // Include Metro library
#define bulb1 1 // Define a LED pin
#define bulb2 3 // Define another LED pin

// Create variables to hold the LED states
int state1 = HIGH;
int state2 = HIGH;

// works fine: 1200/1210

int timeOn1 = 1400;
int timeOn2 = 1390;

int timeOff1 = 1400;
int timeOff2 = 1410;

// Instantiate a metro object and set the interval to 250 milliseconds (0.25 seconds).
Metro metro1 = Metro(1000); 

// Instantiate another metro object and set the interval to 125 milliseconds (0.125 seconds).
Metro metro2 = Metro(1000); 

void setup()
{
  pinMode(bulb1,OUTPUT);
  digitalWrite(bulb1,state1);
  
  pinMode(bulb2,OUTPUT);
  digitalWrite(bulb2,state2);
  
}

void loop()
{

  if (metro1.check() == 1) { // check if the metro has passed its interval .
    if (state1==HIGH)  { 
      state1=LOW;
      metro1.interval(timeOff1);
    } else {
      state1=HIGH;
      metro1.interval(timeOn1);
    }
    digitalWrite(bulb1,state1);
  }
  
   if (metro2.check() == 1) { // check if the metro has passed its interval .
    if (state2==HIGH)  { 
      state2=LOW;
      metro2.interval(timeOff2);
    } else {
      state2=HIGH;
      metro1.interval(timeOn2);
    }
    digitalWrite(bulb2,state2);
  }
  
  
}
